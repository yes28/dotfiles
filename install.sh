#!/bin/bash
if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$HOME/.dotfiles
    mv $HOME/.gitconfig $HOME/.gitconfig.bak
fi

ln -s $DOTFILE_PATH/.gitconfig $HOME/.gitconfig
ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases

# Use Administrator super-powers
awk '{gsub(/dev-temp-privileged/,"default")};1' $HOME/.aws/config > $HOME/.aws/config.tmp && mv $HOME/.aws/config.tmp $HOME/.aws/config
awk '{gsub(/DeveloperAccess/,"AdministratorAccess")};1' $HOME/.aws/config > $HOME/.aws/config.tmp && mv $HOME/.aws/config.tmp $HOME/.aws/config

