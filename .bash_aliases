alias tf=terraform
alias dcr='docker-compose run'
alias dcra='docker-compose run -e DISABLE_SPRING=true api bundle exec'
alias b2m="git checkout master && git pull --rebase origin master"
alias gpo="git push origin"
alias gpfo="git push -f origin"
alias grm="git rebase master"
alias gm="git commit -m"
alias gam="git commit -am"
alias gcb="git checkout -b"
alias gcm="git checkout master"
alias gco="git checkout"
alias gs="git status"
alias switch-to-staging="aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks"
alias switch-to-production="aws --profile production-eks eks update-kubeconfig --name snaptravel-production-eks"
alias switch-to-gitlab="aws --profile gitlab-eks eks update-kubeconfig --name gitlab-runner-cluster"
alias switch-to-gitpod="aws --profile gitpod-eks eks update-kubeconfig --name gitpod-eks-cluster"
alias switch-to-coder="aws --profile coder-eks eks update-kubeconfig --name coder-eks-cluster"
alias enable-repo-creation="curl -v --request PUT --header \"PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN\" --url 'https://gitlab.com/api/v4/groups/616049?project_creation_level=maintainer'"
alias disable-repo-creation="curl -v --request PUT --header \"PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN\" --url 'https://gitlab.com/api/v4/groups/616049?project_creation_level=noone'"

alias psql="docker run -it -v $(pwd):/dump --rm postgres:12 psql"
alias redis-cli="docker run -it -v $(pwd):/dump --rm redis:5 redis-cli"
alias drb='docker run --rm -it --entrypoint bash'